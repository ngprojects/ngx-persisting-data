import {StorageContext} from '../constants/enums/storage-context.enum';
import {IPersistingVariable} from './ipersisting-variable';

export interface IPersistingDataConfigs {
  context?: StorageContext;
  key: string;
  value: any|any[];
  serializable?: boolean;
  expiration?: number;
  onExpiration?: (elem: IPersistingVariable) => any;
  onChange?: (elem: IPersistingVariable) => any;
}
