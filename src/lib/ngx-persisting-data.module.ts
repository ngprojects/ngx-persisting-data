import { NgModule } from '@angular/core';
import {NgxPersistingData} from './ngx-persisting-data';



@NgModule({
  providers: [NgxPersistingData],
  declarations: [],
  imports: [
  ],
  exports: []
})
export class NgxPersistingDataModule { }
