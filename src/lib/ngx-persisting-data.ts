import {IPersistingKeys} from './interfaces/persisting-keys';
import {IPersistingDataConfigs} from './interfaces/ipersisting-data-configs';
import {StorageContext} from './constants/enums/storage-context.enum';
import {Observable, Subject} from 'rxjs';
import {IPersistingSubjects} from './interfaces/ipersisting-subjects';
import {IPersistingVariable} from './interfaces/ipersisting-variable';

/**
 * This library allows you to persist variables globally in your project
 *
 * @author Jesús Cantú
 * @date 11/02/2020
 */
export class NgxPersistingData {
  private data: IPersistingKeys = {};
  private dataListeners: IPersistingSubjects = {};

  /**
   * Returns the current value of an existing item
   * @param key     => item name
   * @param context => storage context
   */
  public get = (key: string, context: StorageContext = StorageContext.Memory, parse: boolean = false): any|any[] => {
    let result;
    switch (context) {
      case StorageContext.Local: result = localStorage.getItem(key); break;
      case StorageContext.Session: result = sessionStorage.getItem(key); break;
      case StorageContext.Memory:
      default: result = this.data[key];
    }
    if (parse) { result = JSON.parse(result); }
    return result;
  }

  /**
   * Returns an observable linked to an existing item
   * @param key     => item name
   * @param context => storage context
   */
    // tslint:disable-next-line:max-line-length
  public getAsObservable = (key: string, context: StorageContext = StorageContext.Memory): Observable<IPersistingVariable> => this.dataListeners[`${context}.${key}`].asObservable();

  /**
   * Sets a new item
   * @param conf => storage configs
   */
  public set = (conf: IPersistingDataConfigs): Observable<IPersistingVariable> => {
    const dLKey = `${conf.context}.${conf.key}`;
    const realValue = conf.value;
    if (conf.serializable) { conf.value = JSON.stringify(conf.value); }
    switch (conf.context) {
      case StorageContext.Local: localStorage.setItem(conf.key, conf.value); break;
      case StorageContext.Session: sessionStorage.setItem(conf.key, conf.value); break;
      case StorageContext.Memory:
      default: this.data[conf.key] = conf.value;
               conf.context = StorageContext.Memory;
    }
    // tslint:disable-next-line:max-line-length
    if (![null, undefined].includes(this.dataListeners[dLKey])) { this.dataListeners[dLKey].next({key: conf.key, value: realValue}); } else {
      this.dataListeners[dLKey] = new Subject<any|any[]>();
      this.dataListeners[dLKey].next({key: conf.key, value: this.get(conf.key, conf.context)});
    }
    if (conf.expiration) {
      setTimeout(() => {
        this.remove(conf.key, conf.context);
        if (conf.onExpiration) { conf.onExpiration({ key: conf.key, value: null }); }
      }, conf.expiration);
    }
    if (conf.onChange) { this.getAsObservable(conf.key, conf.context).subscribe((elem) => conf.onChange(elem)); }
    return this.getAsObservable(conf.key, conf.context);
  }

  /**
   * Removes the specified item
   * @param key     => item name
   * @param context => storage context
   */
  public remove = (key: string, context: StorageContext = StorageContext.Memory): void => {
    const dataListenerKeyName = `${context}.${key}`;
    switch (context) {
      case StorageContext.Local: localStorage.removeItem(key); break;
      case StorageContext.Session: sessionStorage.removeItem(key); break;
      case StorageContext.Memory: delete this.data[key];
    }
    if (this.dataListeners[dataListenerKeyName]) {
      this.dataListeners[dataListenerKeyName].next({key, value: null});
      delete this.dataListeners[dataListenerKeyName];
    }
  }

  /**
   * Removes the whole items in the specified storage context
   * @param context => storage context
   */
  public clear = (context: StorageContext = StorageContext.Memory): void => {
    switch (context) {
      case StorageContext.Local: localStorage.clear(); break;
      case StorageContext.Session: sessionStorage.clear(); break;
      case StorageContext.Memory: this.data = {};
    }
  }

  /**
   * Evaluates the existence of the specified item and executes a callback
   * @param key         => item name
   * @param context     => storage context
   * @param existing    => if exists callback
   * @param nonExisting => if doesn't exists callback
   */
    // tslint:disable-next-line:max-line-length
  public ifExists = (key: string, context: StorageContext, existing: (key?: string, value?: any|any[]) => any, nonExisting?: () => any ): void => {
    if (this.get(key, context)) { existing(key, this.get(key, context)); } else if (nonExisting) { nonExisting(); }
  }
}
